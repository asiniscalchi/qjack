/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef OUTPORT_H
#define OUTPORT_H

#include <QtCore/QDebug>
#include "jackoutport.h"
#include "ringbuffer.h"

namespace QJack{

template <typename T>
class OutPortBase : public JackOutPort
{
public:
    explicit OutPortBase<T>(QString name, quint32 bufferSize);

    quint32 write(const T *data, quint32 nFrames);
    quint32 available() const;
    quint32 writeByte(const char *data, quint32 size);
    quint32 availableByte() const;

private:
    DataStructure::RingBuffer<T> m_buffer;

    void setData(void *data, quint32 nFrames);
};

//! Use OutPort if you haven't particular needings
typedef OutPortBase<jack_default_audio_sample_t> OutPort;

// Definition
template<typename T> OutPortBase<T>::OutPortBase(QString name, quint32 bufferSize):
    JackOutPort(name),
    m_buffer(bufferSize)
{}

template<typename T> void OutPortBase<T>::setData(void *data, quint32 nFrames)
{
    m_buffer.read(reinterpret_cast<T *>(data), nFrames);
}

template<typename T> quint32 OutPortBase<T>::available() const
{
    return(m_buffer.availableWrite());
}

template<typename T> quint32 OutPortBase<T>::write(const T *data, quint32 nFrames)
{
    return(m_buffer.write(data, nFrames));
}

template<typename T> quint32 OutPortBase<T>::availableByte() const
{
    return(m_buffer.availableWrite() * sizeof(T));
}

template<typename T> quint32 OutPortBase<T>::writeByte(const char *data, quint32 size)
{
    if ((size % sizeof(T)) != 0){
        qWarning() << "OutPort | corrupted data";
        return -1;
    }

    return(m_buffer.write(reinterpret_cast<T *>(data), size / sizeof(T)));
}

} // namespace

#endif // OUTPORT_H
