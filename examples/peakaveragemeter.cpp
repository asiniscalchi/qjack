#include "peakaveragemeter.h"
#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

PeakAverageMeter::PeakAverageMeter(quint32 period, QObject *parent):
    QObject(parent)
{
//    cout<<"PeakAverageMeter instantiated with period "<<period<<" samples."<<endl;
    m_samplesperperiod = period;
    m_soundBuffer = new jack_default_audio_sample_t[period];
}

PeakAverageMeter::~PeakAverageMeter()
{
    delete m_soundBuffer;
}

void
PeakAverageMeter::start()
{
//    cout<<"PeakAverageMeter::start() entered."<<endl;
    emit needSoundBuffer(m_soundBuffer,m_samplesperperiod);
}

void
PeakAverageMeter::gotSoundBuffer()
{
    m_peak = 0.;
    m_average = 0.;

    for(quint32 isample=0;isample<m_samplesperperiod;isample++)
    {
        jack_default_audio_sample_t samplsq = m_soundBuffer[isample] * m_soundBuffer[isample];
        m_average += samplsq;
        if(samplsq > m_peak) m_peak = samplsq;
    }

    cout<<setw(6)<<fixed<<setprecision(3);
    cout<<"Peak: "<<(sqrt(m_peak))<<"  Mean: "<<(sqrt(m_average/m_samplesperperiod))<<endl;

    // Ask for a new load of data from whatever is providing it.
    emit needSoundBuffer(m_soundBuffer,m_samplesperperiod);
}

