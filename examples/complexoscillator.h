#ifndef COMPLEXOSCILLATOR_H
#define COMPLEXOSCILLATOR_H

#include <complex>

/*! \brief A sinewave oscillator implemented with complex arithmetic.
 * It maintains a complex phase vector with a length of 1 and rotates it
 * by a complex angle calculated to give a specified frequency based on the
 * sampling rate.
 */
class ComplexOscillator
{
public:
    /*! \brief Basic constructor.
     * \param sampleRate The sample rate of the system in Hz.
     * \param freq The frequency of the desired output in Hz.
     */
    ComplexOscillator(float sampleRate,float freq);

    /*! \brief Return a single sample of the oscillating waveform
     */
    float getSample();

private:
    std::complex<float> phase;        // Phase angle of oscillator.
    std::complex<float> deltaPhi;     // Angle by which to rotate phase each sample.
};

#endif // COMPLEXOSCILLATOR_H
