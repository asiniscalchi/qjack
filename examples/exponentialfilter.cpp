#include "exponentialfilter.h"


ExponentialFilter::ExponentialFilter(float sampleRate, float timeConstant)
{
    // Compute filter constant.
    a = exp(-1./(sampleRate*timeConstant));
    ynm1 = 0.;
}

float ExponentialFilter::filter(float xn)
{
    // Compute one sample of filter and return.
    ynm1 = ynm1 + (1-a)*(xn - ynm1);
    return ynm1;
}
