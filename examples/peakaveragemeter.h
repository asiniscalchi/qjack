#ifndef PEAKAVERAGEMETER_H
#define PEAKAVERAGEMETER_H

#include <jack/jack.h>  // Bring in jack_default_audio_sample_t
#include <QObject>

using namespace std;

//!
//! \brief The PeakAverageMeter class
//! This QObject subclass implements a little peak and average meter for audio signals.
//! The class asks for a bufferfull of audio samples as an array of
//! jack_default_audio_sample_t using a signal specifying the buffer and
//! the number of frames required. The signal returns immediately and the
//! class waits for a slot to be called to tell it when the buffer is full.
//! The slot computes the RMS signal average and peak value then it prints
//! them out on cout. Then it asks for another bufferfull with the signal.
//!
class PeakAverageMeter : public QObject
{
    Q_OBJECT

public:
    //!
    //! \brief PeakAverageMeter Constructor.
    //! \param period The number of samples to compute the peak/average over.
    //! \param parent The QObject parent.
    //!
    explicit PeakAverageMeter(quint32 period, QObject *parent = 0);
    ~PeakAverageMeter();

    //!
    //! \brief start Get the process off the ground. It never stops.
    //!
    void start();

public slots:
    //!
    //! \brief gotSoundBuffer This slot is to be called when the data is ready.
    //!
    void gotSoundBuffer();

signals:
    //!
    //! \brief needSoundBuffer This signal indicates readiness for more data.
    //!
    void needSoundBuffer(jack_default_audio_sample_t *, quint32 );

private:
    //!
    //! \brief m_peak Where we keep track of the peak signal.
    //!
    jack_default_audio_sample_t m_peak;

    //!
    //! \brief m_average Where we keep track of the squared mean.
    //!
    jack_default_audio_sample_t m_average;

    //!
    //! \brief m_samplesperperiod Number of frames in the buffer.
    //!
    quint32 m_samplesperperiod;

    //!
    //! \brief m_soundBuffer The buffer where the data is.
    //!
    jack_default_audio_sample_t *m_soundBuffer;
};

#endif // PEAKAVERAGEMETER_H

