#include "qfillbufferinport.h"

#include <iostream>
using namespace std;

namespace QJack{


QFillBufferInPort::QFillBufferInPort(QString name, quint32 buffersize, QObject *parent) :
    QObject(parent), InPort(name, buffersize)
{
//    cout<<"QFillBufferInPort constructed. Name: "<<qPrintable(name)<<" buffersize: "<<buffersize<<endl;
    m_fillBuffer = 0;   // If no buffer it just queues data in the ringbuffer.
}

QFillBufferInPort::~QFillBufferInPort()
{
    delete m_fillBuffer;
}

void
QFillBufferInPort::fill(jack_default_audio_sample_t *buffer, quint32 nframes)
{
//    cout<<"QFillBufferInPort::fill() entered."<<endl;
    m_fillBuffer = buffer;
    m_fillBufferSize = nframes;
    m_filledFrames = 0;

    fillPartially();
}

void
QFillBufferInPort::fillPartially()
{
    if(m_fillBuffer)    // Do we have a buffer to fill at all?
    {
        quint32 lefttofill = m_fillBufferSize - m_filledFrames;
        quint32 availtofill = InPort::available();

        if( availtofill < lefttofill )
        {
            // Not enough to completely fill it.
            InPort::read(&m_fillBuffer[m_filledFrames],availtofill);
            m_filledFrames += availtofill;
        }
        else
        {
            // Finally have enough to complete the fill.
            InPort::read(&m_fillBuffer[m_filledFrames],lefttofill);
            m_fillBuffer = 0;   // No more buffer to fill.

            // Signal that the buffer is now filled.
            emit filled();
        }
    }
    // Quietly exit without doing anything, while InPort collects more data.
}

void
QFillBufferInPort::setData(void *data, quint32 nFrames)
{
    // Call the superclass setData method to load data into ringbuffer
    InPort::setData(data, nFrames);
//     cout<<"setData()"<<endl;
    fillPartially();
}

} // namespace QJack
