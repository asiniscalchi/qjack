/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INPORT_H
#define INPORT_H

#include <QtCore/QObject>
#include <QtCore/QDebug>
#include "jackinport.h"
#include "ringbuffer.h"

namespace QJack {

template <typename T>
class InPortBase : public JackInPort
{
public:
    explicit InPortBase<T>(QString name, quint32 bufferSize);

    quint32 read(T *data, quint32 nFrames);
    quint32 available() const;
    quint32 readByte(char *data, quint32 size);
    quint32 availableByte() const;

private:
    DataStructure::RingBuffer<T> m_buffer;

protected:
    void setData(void *data, quint32 nFrames);
};

//! Use InPort if you haven't particular needings
typedef InPortBase<jack_default_audio_sample_t> InPort;

// Definition
template <typename T> InPortBase<T>::InPortBase(QString name, quint32 bufferSize):
JackInPort(name),
m_buffer(bufferSize)
{}

template <typename T> void InPortBase<T>::setData(void *data, quint32 nFrames)
{
    m_buffer.write(reinterpret_cast<T *>(data), nFrames);
}

template <typename T> quint32 InPortBase<T>::available() const
{
    return(m_buffer.availableRead());
}

template <typename T> quint32 InPortBase<T>::read(T *data, quint32 nFrames)
{
    return(m_buffer.read(data, nFrames));
}

template <typename T> quint32 InPortBase<T>::availableByte() const
{
    return(m_buffer.availableRead() * sizeof(T));
}

template <typename T> quint32 InPortBase<T>::readByte(char *data, quint32 size)
{
    if ((size % sizeof(T)) != 0){
        qWarning() << "InPort | corrupted data";
        return -1;
    }

    return(m_buffer.read(reinterpret_cast<T *>(data), size / sizeof(T)));
}

} // namespace

#endif // InPort_H
