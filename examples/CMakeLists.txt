PROJECT(jexamples)

if(NOT FORCE_QT4)
    find_package(Qt5Core QUIET)
endif()
if(NOT Qt5Core_FOUND)
    find_package(Qt4 REQUIRED)
    include(${QT_USE_FILE})
    include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR})
endif()

find_package(Jack REQUIRED)


# Executable "example"
add_executable(example example.cpp)
target_link_libraries(example ${QT_LIBRARIES} qjack)
if(Qt5Core_FOUND)
    qt5_use_modules(example Core)
endif()

# Executable "qoutportexample"
add_executable(qoutportexample qoutportexample.cpp noiseqoutport.cpp complexoscillator.cpp toneqoutport.cpp)
target_link_libraries(qoutportexample ${QT_LIBRARIES} qjack)
if(Qt5Core_FOUND)
    qt5_use_modules(qoutportexample Core)
endif()

# Executable "qinportexample"
add_executable(qinportexample qinportexample.cpp rmsqinport.cpp exponentialfilter.cpp )
target_link_libraries(qinportexample ${QT_LIBRARIES} qjack)
if(Qt5Core_FOUND)
    qt5_use_modules(qinportexample Core)
endif()

# Executable "qfillbufferexample" - peakaveragemeter is a QObject so generate MOC
set(jexamples_MOC_TARGET
   peakaveragemeter.h
)

if(NOT FORCE_QT4)
    find_package(Qt5Core QUIET)
endif()

if(Qt5Core_FOUND)
    qt5_wrap_cpp(jexamples_MOC_GENERATED ${jexamples_MOC_TARGET})
else()
    find_package(Qt4 COMPONENTS QTCORE REQUIRED)
    include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR})
    qt4_wrap_cpp(jexamples_MOC_GENERATED ${jexamples_MOC_TARGET})
endif()

add_executable(qfillbufferexample qfillbufferexample.cpp peakaveragemeter.cpp ${jexamples_MOC_GENERATED})
target_link_libraries(qfillbufferexample ${QT_LIBRARIES} qjack)

if(Qt5Core_FOUND)
    qt5_use_modules(qfillbufferexample Core)
endif()

