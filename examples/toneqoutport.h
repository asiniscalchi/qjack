#ifndef TONEQOUTPORT_H
#define TONEQOUTPORT_H

#include "qjack/qoutport.h"

#include "complexoscillator.h"

/*! \brief A QJack QOutPort that outputs a sinewave tone.
 * This class overrides the setData method providing Jack with sinewave
 * data from a ComplexOscillator when it asks for it.
 */
class ToneQOutPort: public QJack::QOutPort
{
public:
    /*! \brief Constructor.
     * \param name The Jack port name.
     * \param bufferSize The number of frames requested by Jack.
     * \param samplerate The Jack sample rate in Hz.
     * \param freq The frequency of the tone desired in Hz.
     * \param parent The QObject parent object.
     */
    ToneQOutPort(QString name, quint32 bufferSize, float samplerate, float freq, QObject *parent);
    ~ToneQOutPort();

    /*! \brief This method is called by Jack when data is required.
     * \param data A void pointer to the data buffer from jack.
     * \param nFrames The number of frames to put into the buffer.
     */
    void setData(void *data, quint32 nFrames);

private:
    ComplexOscillator *cosc;
};

#endif // TONEQOUTPORT_H
