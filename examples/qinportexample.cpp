#include <iostream>
#include <iomanip>
#include <math.h>

#include "qjack/qjackclient.h"
#include "qjack/qinport.h"

#include "rmsqinport.h"


using namespace std;
using namespace QJack;

/*! \brief An RMS reading signal amplitude meter.
 * This is a little demo of a QJack program that accepts some input
 * and processes it. In this case, the processing is to calculate an
 * RMS amplitude and display it with an ASCII-art meter on a console.
 */
int main(int argc, char** argv)
{
    cout<<"QInPortExample starting."<<endl;

    // Allocate a Jack client and name it.
    QJackClient* client = new QJackClient("QInPortExample");

    // Open the client or bust.
    if (false == client->open()){
        exit(EXIT_FAILURE);
    }

    // Create an RmsQInPort with a buffer with 2 * number of frames JACK will supply
    RMSQInPort *rmsport = new RMSQInPort("input", client->nFrames()*2, client->sampleRate(), .1, client);
    client->addPort(rmsport);

    // Get the client off the runway.
    client->activate();

    cout << "Connecting newly created input port to system output port." << endl;
    QStringList outportnames = client->getPorts( NULL, NULL, (JackPortFlags)(JackPortIsOutput|JackPortIsPhysical) );

    if( outportnames.size() > 0 )    // Make sure there is at least one system output port.
    {
        if( 0 != client->connect(outportnames[0],rmsport) )   // Make the connection.
        {
            cout << "Failed to connect system output port to rms meter port." << endl;
        }
    }
    else
    {
        cout << "No output port available!" << endl;
    }

    cout<<"Starting loop. Hit interrupt to exit ..."<<endl<<endl;

    int meterwidth = 50;    // How wide is the meter display?
    int meterzero = 40;     // Where is 1.0 amplitude located?

    cout<<setprecision(5)<<fixed;
    while(true)
    {
        float rmsamp = rmsport->rmsAmplitude();
        cout<<"RMS: "<<rmsamp<<"  ";

        int metervalue = rmsamp*meterzero;
        if (metervalue > meterwidth-1) metervalue = meterwidth-1;

        for(int imeter=0;imeter<meterwidth;imeter++)
        {
            if( imeter < metervalue ) cout<<"}";
            else if( imeter == meterzero ) cout<<"+";
            else cout<<"-";
        }
        cout<<"\r";
        cout.flush();
    }

    return (0);
}
