#ifndef NOISEQOUTPORT_H
#define NOISEQOUTPORT_H

#include "qjack/qoutport.h"

/*! \brief A QOutPort that produces white noise.
 * This class inherits from QOutPort and implements the setData method
 * to send a buffer full of random values when Jack asks for data.
 */
class NoiseQOutPort: public QJack::QOutPort
{
public:
    /*! \brief The only useful contructor.
     * \param name The name of the resulting Jack port.
     * \param bufferSize How many frames Jack wants.
     * \param parent The QObject parent.
     */
    NoiseQOutPort(QString name, quint32 bufferSize, QObject *parent);

    /*! \brief Called when Jack requires data.
     * \param data A void pointer to the data buffer from Jack.
     * \param nFrames The number of frames that Jack requires.
     */
    void setData(void *data, quint32 nFrames);
};

#endif // NOISEQOUTPORT_H
